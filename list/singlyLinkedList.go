package list

import "fmt"

type node struct {
	data int
	next *node
}

type linkedList struct {
	head *node
	length int
}
func CreateNode(v int) *node {
	return &node{data: v}
}
func CreateSinglyLinkedList() *linkedList {
	return &linkedList{}
}

func (l linkedList) Length() int {
	return l.length
}

func (l *linkedList) Prepend(n *node) {
	n.next = l.head
	l.head = n
	l.length++
}

func (l *linkedList) Delete(v int) {
	prevNode := l.head
	currNode := l.head
	for currNode != nil {
		if currNode.data == v {
			if currNode == l.head {
				l.head = currNode.next
			} else {
				prevNode.next = currNode.next
			}
			l.length--
			break
		}
		prevNode = currNode
		currNode = currNode.next
	}
}

func (l *linkedList) Println() {
	currNode := l.head
	for currNode != nil {
		fmt.Printf("%d ", currNode.data)
		currNode = currNode.next
	}
	fmt.Println("")
}