package list

import (
	"reflect"
	"testing"
)

func TestCreateNode(t *testing.T) {
	t.Run("should create a node with supplied value", func(t *testing.T) {
		want := &node{data:10}
		if got := CreateNode(10); !reflect.DeepEqual(got, want) {
			t.Errorf("CreateNode() = %v, want %v", got, want)
		}
	})
}

func TestCreateSinglyLinkedList(t *testing.T) {
	t.Run("should create a empty linked list", func(t *testing.T) {
		want := &linkedList{}
		if got := CreateSinglyLinkedList(); !reflect.DeepEqual(got, want) {
			t.Errorf("CreateNode() = %v, want %v", got, want)
		}
	})
}

func TestPrepend(t *testing.T) {
	t.Run("should add a node to the linked list", func(t *testing.T) {
		n1 := CreateNode(18)
		l := CreateSinglyLinkedList()
		want := 18
		l.Prepend(n1)
		if !reflect.DeepEqual(l.head.data, want) {
			t.Errorf("Prepend() = %v, want %v", l.head.data, want)
		}
	})
}

func TestLength(t *testing.T) {
	t.Run("should return 0 as the length of the empty linked list", func(t *testing.T) {
		l := CreateSinglyLinkedList()
		want := 0
		got:= l.Length()
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Length() = %v, want %v", got, want)
		}
	})

	t.Run("should return the length of the linked list when nodes are added", func(t *testing.T) {
		n1 := CreateNode(28)
		n2 := CreateNode(18)
		n3 := CreateNode(8)
		l := CreateSinglyLinkedList()
		l.Prepend(n1)
		l.Prepend(n2)
		l.Prepend(n3)
		want := 3
		got:= l.Length()
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Length() = %v, want %v", got, want)
		}
	})

	t.Run("should return the length of the linked list when nodes are deleted", func(t *testing.T) {
		n1 := CreateNode(28)
		n2 := CreateNode(18)
		n3 := CreateNode(8)
		l := CreateSinglyLinkedList()
		l.Prepend(n1)
		l.Prepend(n2)
		l.Prepend(n3)
		want := 3
		got:= l.Length()
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Length() = %v, want %v", got, want)
		}
		l.Delete(18)
		got = l.Length()
		want = 2
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Length() = %v, want %v", got, want)
		}
	})
}

func TestDelete(t *testing.T) {
	t.Run("should be able to handle delete operation from an empty linked list", func(t *testing.T) {
		l := CreateSinglyLinkedList()
		l.Delete(18)
		if l.head != nil {
			t.Errorf("Delete() = %v, want %v", l.head, nil)
		}
	})

	t.Run("should delete a node from the linked list", func(t *testing.T) {
		n1 := CreateNode(28)
		n2 := CreateNode(18)
		n3 := CreateNode(8)
		l := CreateSinglyLinkedList()
		l.Prepend(n1)
		l.Prepend(n2)
		l.Prepend(n3)
		l.Delete(18)
		want := 8
		if !reflect.DeepEqual(l.head.data, want) {
			t.Errorf("Delete() = %v, want %v", l.head.data, want)
		}
	})

	t.Run("should not delete anything linked list if the value does not exist", func(t *testing.T) {
		n1 := CreateNode(18)
		n2 := CreateNode(28)
		l := CreateSinglyLinkedList()
		l.Prepend(n1)
		l.Prepend(n2)
		l.Delete(100)
		want := 28
		if !reflect.DeepEqual(l.head.data, want) {
			t.Errorf("Delete() = %v, want %v", l.head.data, want)
		}
	})

	t.Run("should delete the first node of linked list", func(t *testing.T) {
		n1 := CreateNode(8)
		n2 := CreateNode(18)
		n3 := CreateNode(28)
		l := CreateSinglyLinkedList()
		l.Prepend(n1)
		l.Prepend(n2)
		l.Prepend(n3)
		l.Delete(28)
		got := l.head.data
		want := 18
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Delete() = %v, want %v", got, want)
		}
	})
}


